Markov-id
=========

encode and decode numbers as text using markov chains


Usage
------
```
var MarkovId = require('./markov-id');
var markov = new MarkovId(text, order);
var string = markov.getString(1337);
markov.decodeString(string);
//should return 1337
```
