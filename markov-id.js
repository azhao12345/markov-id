var MarkovId = function(text, order)
{
    this.data = {};
    this.order = order;

    //this creates a ring of text so that we won't have dead ends
    text = text + text.substring(0, order);
  
    //process the input string and turn it into a graph
    for(var i = 0; i < text.length - order; i++)
    {
        var key = text.substring(i, i + order);
        if(!this.data[key])
        {
            this.data[key] = {letter: key.charAt(key.length - 1), values: []};
        }
        var value = text.substring(i + 1, i + order + 1);
        if(!this.data[value])
        {
            this.data[value] = {letter: value.charAt(value.length - 1), values: []};
        }
        if(this.data[key].values.indexOf(this.data[value]) == -1)
        {
            this.data[key].values.push(this.data[value]);
        }
    }

    //calculate reverse map for decoding the string into a number
    var items = Object.getOwnPropertyNames(this.data);

    for(i = 0; i < items.length; i++)
    {
        var values = this.data[items[i]].values;
        var indexes = {};
        this.data[items[i]].indexes = indexes;
        for(var j = 0; j < values.length; j++)
        {
            indexes[values[j].letter] = j;
        }
    }
};

module.exports = MarkovId;

MarkovId.prototype.getString = function(value)
{
    var starts = Object.getOwnPropertyNames(this.data);

    var startIndex = value % starts.length;
    var startValue = (value - startIndex) / starts.length;
    
    var startString = starts[startIndex];

    var startState = this.data[startString];
    return startString.substring(0, startString.length - 1) + stringHelper(startState, startValue);
};

//this recursively assembles the string
function stringHelper(state, value)
{
    //We will connect a ring from the end of the text back to the beginning
    //This will result in some silliness, but it removes all dead ends
    if(value === 0)
    {
        return state.letter;
    }
    //We will use modulus division to make this work
    var nextStateIndex = value % state.values.length;
    var nextValue = (value - nextStateIndex) / state.values.length;
    return state.letter + stringHelper(state.values[nextStateIndex], nextValue);
}

MarkovId.prototype.decodeString = function(string)
{
    var startString = string.substring(0, this.order);

    //get the index of this start string
    var starts = Object.getOwnPropertyNames(this.data);

    var index;
    for(var i = 0 ; i < starts.length; i++)
    {
        if(starts[i] == startString)
        {
            index = i;
            break;
        }
    }
    
    return index + starts.length * decodeHelper(this.data[startString], string, this.order);
};

function decodeHelper(state, str, startidx)
{
    //get the index of our state transition
    var index = state.indexes[str.charAt(startidx)];
    if(startidx == str.length - 1)
    {
        return index;
    }
    else
    {
        return index + state.values.length * decodeHelper(state.values[index], str, ++startidx);
    }
}
